# <img src ="monopoly/images/money_bag_icon.png" width="50" heigth="50"> Project 03-Monopoly

This project represents board game Monopoly with famous Serbian locations and streets of Belgrade.  

# <img src = "monopoly/screenshots/example.png">

# About

  Monopoly is a famous board game where the goal is to own as many properties with houses or hotels on them, and be the richest 
  player in the game. Properties can be bought, sold, mortgaged or you can build estates on them. With each estate, property's rent rises.
  There are four players in the game including you. Other players that step on your property pay you current rent of that property.
  There are 4 special fields: GO, JAIL, FREE PARKING and GO TO JAIL. Each time a player passes GO field recieves salary. If player steps
   on JAIL field, he is just visiting, but if he steps to GO TO JAIL field, he must go to jail and next turn he must pay the fine to get out
   of the jail. If player steps on FREE PARKING field, he is just visiting and doesn't need to pay anything to the bank. 
  Player can also step on Community or Chance fields. In those cases, he draws a Community or Chance card from their deck and he must do what is
   written on them. Card is put back at the bottom of the deck at end of player's turn.

  You are the winner if all other players go bankrupt. 
  
  Tutorial:
- [How to play](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/03-monopol/-/wikis/Tutorial)

# Requirements and run Monopoly

- Qt version 5.12 or up
- [How to run game](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/03-monopol/-/wikis/Requirements-and-compiling)

# Demo video

  https://youtu.be/BL5SGmYmFfU

# Weekly reports:
- [Reports](https://gitlab.com/matf-bg-ac-rs/course-rs/projects-2020-2021/03-monopol/-/wikis/Reports)
## Developers

- [Miloš Milaković, 152/2017](https://gitlab.com/Milak9)
- [Ana Nikačević, 272/2017](https://gitlab.com/AnaNika)
- [Milica Radojičić, 131/2017](https://gitlab.com/milicar7)
- [Marija Lakić, 84/2017](https://gitlab.com/marijal74)
