#ifndef BOT_HPP
#define BOT_HPP

#include <iostream>
#include <string>
#include <vector>
#include <QRandomGenerator>
#include <algorithm>
#include "player.hpp"

enum class Decision { sell_property, build, sell_estate,mortgage,sell_all_estates,nothing };

class Bot : public Player
{
    public:
        Bot( unsigned id, QString name, unsigned position = 0, int budget = 0);

        bool decide_pay_fine_or_jail_card();
        double decide_to_lift_mortgage(int mortgage_with_interest);
        bool decide_to_pay_tax();
        Decision decide_for_property(Field &f);
        bool decide_to_buy_property(Field &f);
        double decide_mortgage_property(Field &f);
        double decide_build_estate(Field &f);
        double decide_sell_property(Field &f);
        double decide_sell_one_estate(Field &f);
        double decide_sell_all_estates(Field &f);
        int decide_which_property_to_mortgage(int sum);

    private:

        int number_of_mortgages();
        double iq;
        int min_budget;
        QRandomGenerator random = *QRandomGenerator::global();

};

#endif // BOT_HPP
