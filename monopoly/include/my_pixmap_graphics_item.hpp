#ifndef MY_PIXMAP_GRAPHICS_ITEM_HPP
#define MY_PIXMAP_GRAPHICS_ITEM_HPP
#include <QGraphicsItem>
#include <QObject>
class My_pixmap_graphics_item:  public QObject, public QGraphicsPixmapItem
{
    Q_OBJECT
public:

   My_pixmap_graphics_item(const QPixmap & pixmap, QGraphicsItem * parent = 0 );

public slots:
    void hide();

protected:
};

#endif // MY_PIXMAP_GRAPHICS_ITEM_HPP
