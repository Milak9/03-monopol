#ifndef PLAYER_HPP
#define PLAYER_HPP

#include <QGraphicsItem>
#include <QPainter>
#include <QGraphicsScene>
#include <QStyleOption>
#include <QMap>

#include <iostream>
#include <string>
#include <vector>
#include <numeric>
#include <cstdlib>

#include "field.hpp"
#include "rectangle_with_text.hpp"
#include "card_property.hpp"
#include "card_chance_community.hpp"
#include "field.hpp"

class Player
{
public:

        Player( unsigned id, QString name, unsigned position = 0,
                int budget = 0);
        virtual ~Player();

        bool operator=(const Player *other) const {return other->id == id; }

        void set_jail_card(Field_kind kind);

        void set_number_of_houses(const bool build);
        void set_number_of_hotels(const bool build);
        Field_kind unset_jail_card();
        int get_number_of_houses() {return number_of_houses_;}
        int get_number_of_hotels() {return number_of_hotels_;}

        void set_jail(const bool in) {in_jail = in;}
        bool check_jail() const {return in_jail;}
        QString get_name() const {return  name;}

        void sell_property( Field& f);
        std::pair<unsigned, bool> move_forward(const int steps);
        void receive_money(const int sum);
        bool check_id(const unsigned id) {return this->id==id;}


        void add_card(const Card_Chance_Community& c);
        bool check_jail_card() const;
        bool check_possesion(const unsigned id) const;
        bool has_estates(const QString color) const;
        bool pay_to_bank(int sum);
        bool pay_mortgage(int mortgage);
        void update_num_estates(bool house);
        std::vector<Field*> return_color_group(const QString color);
        //checks all restrictions for building/selling estates
        bool can_build_sell( Field f, const bool build);
        //returns if the estate sold is house
        bool sell_estate( Field& f, bool all=false);
        bool pay_to_player(Player* p, const int sum);
        bool  add_property(Field& p);
        void sell_all_estates();
        int calculate_total_worth() const;

        unsigned get_id() {return id;}

        void doubles() {num_doubles++;}
        void reset_doubles() {num_doubles = 0;}
        unsigned get_doubles() {return num_doubles;}

        int get_position() const {return position_;}
        int get_budget() const {return budget_;}
        std::vector<Field*> get_fields() {return fields_;}


        int get_needed_money() const {return needed_money_;}
        void set_needed_money(int needed_money) {needed_money_ = needed_money;}
        void set_bankrupted(bool value) {bankrupted = value;}
        bool is_bankrupted() const {return bankrupted;}


        //---------------ZA RECTANGLES WITH TEXT---------------------------
        QMap<QString, Rectangle_with_text *>get_owned_rectangles_with_text() const {return owned_rectangles_with_text_;}
        void add_new_rectnagle_with_text(Rectangle_with_text*);
        void remove_rectangle_with_text(QString);
        Rectangle_with_text* find_rectangle_with_text(QString name);
        void disable_all_rectangles_with_text(bool val);



private:

        QMap<QString,Rectangle_with_text*> owned_rectangles_with_text_;

protected:
        bool jail_card = false;
        Field_kind jail_card_from;
        unsigned num_doubles = 0;
        int position_ = 0;
        int budget_;
        int needed_money_ = 0;
        int total_worth;
        bool in_jail = false;
        bool bankrupted = false;
        int number_of_houses_ = 0;
        int number_of_hotels_ = 0;
        std::vector<Field*> fields_;
        QString name;
        unsigned id;
        QColor color_;
};


#endif // PLAYER_HPP
