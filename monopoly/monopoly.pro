QT += core gui
QT += widgets
QT += multimedia

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += console c++17
#QT -= core gui
#TEMPLATE = app
#CONFIG += console

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    src/game_graphics.cpp \
    src/game_logic.cpp \
    src/game_over.cpp \
    src/rules.cpp \
    src/pause.cpp \
    src/button.cpp \
    src/game.cpp \
    src/main.cpp \
    src/bank.cpp \
    src/card_chance_community.cpp \
    src/card_property.cpp \
    src/board.cpp \
    src/field.cpp \
    src/player.cpp \
    src/bot.cpp\
    src/notification.cpp \
    src/animate_pixmap.cpp \
    src/animate_player.cpp \
    src/my_pixmap_graphics_item.cpp \
    src/rectangle_with_text.cpp \
    src/welcome.cpp \
    src/keyPressEventFilter.cpp \
    src/game_tax.cpp \
    src/game_jail.cpp \
    src/game_mortgage.cpp \
    src/game_options.cpp \

HEADERS += \
    include/game_over.h \
    include/button.hpp \
    include/game.hpp \
    include/bank.hpp \
    include/card_chance_community.hpp \
    include/card_property.hpp \
    include/board.hpp \
    include/field.hpp \
    include/my_pixmap_graphics_item.hpp \
    include/rectangle_with_text.hpp \
    include/player.hpp \
    include/bot.hpp \
    include/notification.hpp \
    include/animate_pixmap.hpp \
    include/animate_player.hpp \
    include/welcome.hpp \
    include/rules.h \
    include/pause.h \
    include/keyPressEventFilter.h

FORMS += \
    forms/rules.ui \
    forms/welcome.ui \
    forms/game_over.ui \
    forms/pause.ui
RESOURCES += \
    resources.qrc

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
