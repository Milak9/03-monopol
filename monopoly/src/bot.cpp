#include "../include/bot.hpp"

Bot::Bot( unsigned id, QString name, unsigned position,int budget)
    : Player(id, name, position, budget){
    iq = random.generateDouble();
    min_budget = random.bounded(50,500);
}

//returns true if player decides to get out of jail with card
bool Bot::decide_pay_fine_or_jail_card(){

    if(budget_ < 50 && check_jail_card()){
        return true;
    }
    else if (budget_ - 50 < min_budget){
        return random.generateDouble() * iq > 0.35 ? true:false;
    }
    else
        return random.generateDouble() * iq > 0.65 ? true:false;

}

//returns true if player wants to lift the mortgage
double Bot::decide_to_lift_mortgage(int mortgage_with_interest){

    // player doesn't have enough money to lift the mortgage
    if (budget_ - mortgage_with_interest <= 0 )
        return 0;
    // higher chances to choose to pay of the mortgage, bc it's small
    int factor = iq * random.generateDouble();
    if (mortgage_with_interest < 100){
        return factor + 0.75;
    }
    // lesser chances, player will remain with amount less than min_budget
    // which represents the least amount of money he would like to have during the whole game
    if (budget_ - mortgage_with_interest < min_budget)
        return factor + 0.45;
    else    //  higher chances
        return iq * random.generateDouble() + 0.7;


}

//returns true if player wants to pay 10% of total worth, else 200e
bool Bot::decide_to_pay_tax(){

   int percent_of_total_worth = 0.1 * this->calculate_total_worth();
     // int percent_of_total_worth = 0.1 * 500;
    if(budget_ == 0){
        return false;
    }
    else if(budget_ - percent_of_total_worth > budget_ - 200)
        return (iq * random.generateDouble() > 0.15) ? true:false;
    else
        return (iq * random.generateDouble() > 0.85) ? true:false;
    //return false;

}

Decision Bot::decide_for_property(Field &f){

 std::vector<double> options;
    options.push_back(decide_sell_property(f));
    options.push_back(decide_mortgage_property(f));
    if(f.get_kind()==Field_kind::PROPERTY){
        options.push_back(decide_sell_all_estates(f));
        options.push_back(decide_sell_one_estate(f));
        options.push_back(decide_build_estate(f));
    }

    double winner = std::accumulate(options.begin(), options.end(), 0.0, [](auto a, auto b) {return (a>b) ? a:b;});

    double factor = random.generateDouble();

    if (winner == *options.begin() && factor * iq * winner > 0.2)
        return Decision::sell_property;
    else  if(winner == *(options.begin() + 1) && factor * iq * winner > 0.2)
        return Decision::mortgage;


    if(f.get_kind()==Field_kind::PROPERTY){
        if (winner == *(options.begin() + 2) && factor * iq * winner > 0.2)
            return Decision::sell_all_estates;
        else if (winner == *(options.begin() + 3) && factor * iq * winner > 0.2)
            return Decision::sell_estate;
        else if (winner == *(options.begin() + 4) && factor * iq * winner > 0.2)
            return Decision::build;
        else
            return Decision::nothing;
    }
    else
        return Decision::nothing;   // do nothing

}

// depends of number od same colored fields in possesion, difference
// between remained budget and min_budget,
// how much cash player has in contrast of total_worth
bool Bot::decide_to_buy_property(Field &f){

    int num_of_color_in_possesion = size(this->return_color_group(f.get_color()));

    if (num_of_color_in_possesion >= 2 && budget_ - f.get_price() > min_budget){
        return (iq*random.generateDouble() > 0.05) ? true:false;
    }
    else if(budget_ - f.get_price() <= min_budget){
        return (iq*random.generateDouble() > 0.40) ? true:false;
    }
    else if(this->calculate_total_worth() > budget_ * 3){
        return (iq*random.generateDouble() > 0.45) ? true:false;
    }
    else{
        return (iq*random.generateDouble() > 0.10) ? true:false;
    }
    //return true;
}

double Bot::decide_mortgage_property(Field &f) {return 1 - decide_to_lift_mortgage(f.get_mortgage());}

int Bot::number_of_mortgages(){
    int num_mortgages = 0;

    for(auto f:fields_){
        if(f->is_mortgaged())
            num_mortgages++;
    }
    return num_mortgages;

}

// Decide to build anything (house or hotel) other functions will deal with what will be
double Bot::decide_build_estate(Field &f)
{
    double factor = random.generateDouble();

    if (budget_ - f.get_house_price() > min_budget)
        return iq * factor + 0.4;
    else
        return iq * factor; 
}

int Bot::decide_which_property_to_mortgage(int sum){

    std::vector<bool> probs;
    if(fields_.empty())
        return -1;

    for(auto f:this->fields_){

        double factor = random.generateDouble() * iq;
        if(f->get_num_of_houses()==0 && !f->has_hotel()){
            probs.push_back(factor > 0.05 ? true:false);
        }
        else if(f->has_hotel()){
            probs.push_back(factor > 0.75 ? true:false);
        }
        else if(f->get_num_of_houses() > 2){

            probs.push_back(factor > 0.12 ? true:false);

        }
        else if(budget_ - sum > min_budget && f->get_mortgage() > sum){
            probs.push_back(factor > 0.02 ? true:false);
        }
        else{
            probs.push_back(factor > 0.06 ? true:false);
        }

    }

    int chosen_id = -1;
    int max_factor = 0;
    for(int i = 0; i < (int)fields_.size(); i++){
           std::cout<<"*****PROBS OD "<<i<<" "<<probs[i]<<std::endl;
        if(probs[i]){

            auto f = decide_mortgage_property(*fields_[i]);
            if(f > max_factor){
                max_factor = f;
                chosen_id = fields_[i]->get_id();
            }
        }
    }
    std::cout<<"*****CHOSEN ID JE posle fora "<<chosen_id<<std::endl;

    if(max_factor == 0){
        chosen_id = random.generate64() * fields_.size() / QRandomGenerator::max();
        return chosen_id;
    }
    std::cout<<"*****CHOSEN ID JE posle ifa "<<chosen_id<<std::endl;
    return chosen_id;



}
// Less probability to sell estate if there are houses or hotel on it
double Bot::decide_sell_one_estate(Field &f)
{

    auto clr_grp = return_color_group(f.get_color());
    bool has_hotel = false;
    int num_of_houses = 0;

    for(auto clr_f:clr_grp){
        has_hotel = clr_f->has_hotel();
        if(has_hotel){
            break;
        }
        int clr_num_of_houses = clr_f->get_num_of_houses();
        if(num_of_houses < clr_num_of_houses){
            num_of_houses = clr_num_of_houses;
        }

    }
    double factor = random.generateDouble();
    if (has_hotel)
        return iq * factor + 0.1;
    else if (num_of_houses == 4)
        return iq * factor + 0.2;
    else if (num_of_houses == 3)
        return  iq * factor + 0.3;
    else if (num_of_houses == 2 )
        return iq * factor + 0.4;
    else if (num_of_houses == 1)
        return iq * factor + 0.5;
    else
        return iq * factor;
}

double Bot::decide_sell_all_estates(Field &f)
{
    auto clr_grp = return_color_group(f.get_color());
    int sum = 0;

    int num_of_estates = 0;

    for(auto field:clr_grp){
        if(field->has_hotel()){
            sum += field->get_hotel_price() / 2;
            num_of_estates++;
        }
        else{
            sum += field->get_house_price() / 2 * field->get_num_of_houses();
            num_of_estates += field->get_num_of_houses();
        }

    }
    auto factor = random.generateDouble() * iq;
    if(budget_ < min_budget && budget_ + sum > min_budget){
        return factor + 0.7;
    }
    else if(num_of_estates > 4){
        return factor - 0.2;
    }
    else{
        return factor + 0.1;
    }
}

double Bot::decide_sell_property(Field &f)
{
    int num_of_color_in_possesion = size(this->return_color_group(f.get_color()));

    double factor = iq*random.generateDouble();

    if (num_of_color_in_possesion >= 2 && budget_ - f.get_price() > min_budget){
        return factor + 0.85;
    }
    else if(budget_ + f.get_price() <= min_budget){
        return factor + 0.15;
    }
    else if(this->calculate_total_worth() > budget_ * 3){
        return factor + 0.55;
    }
    else{
        return factor + 0.30;
    }
 }
