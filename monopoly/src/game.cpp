#include "../include/game.hpp"
void Game::play_game(){

    if(throw_dices_again){
        advance_player(*curr_player_, getBoard()->getCurrentPlayer(0)->advance());
    }
    else{
        throw_dices_again=true;
    }
    
    unsigned int field_id = getBoard()->getCurrentPlayer(0)->position();
    curr_field_ = board_->get_field_by_id(field_id);
    QString name = curr_player_->get_name();
    Field_kind kind = curr_field_->get_kind();
    if(kind == Field_kind::GO_TO_JAIL){
        std::cout << "USAO U GO TO JAIL" << std::endl;
        emit stepped_on_go_to_jail();
     }
    else if(kind == Field_kind::CHANCE || kind == Field_kind::COMMUNITY){
            std::cout << "USAO U CHANCE ILI COMMUNITY" << std::endl;
            step_on_chance_community_field(curr_player_, *curr_field_);
     }
    else if(kind == Field_kind::PROPERTY){


      std::cout << "PROPERTY" <<std::endl;
      std::cout<< name.toStdString() << " stepped on " << curr_field_->get_name().toStdString() << std::endl;
      Owner owner = curr_field_->get_owner();
      if (owner == Owner::BANK){
                emit stepped_on_property();
       }
      else if(id_to_owner(curr_player_->get_id()) != curr_field_->get_owner()){
          emit stepped_on_another_players_property();
       }
       else
           {
           if(curr_field_->is_mortgaged())
             {
                  emit do_you_want_to_lift_the_mortgage();
              }
           else
             {
               emit stepped_on_previously_bought_field();
             }

           }

    }
   else if(kind == Field_kind::TAX){
            std::cout << "USAO U TAX" << std::endl;
            emit stepped_on_tax();
   }
  else if(kind == Field_kind::SUPPLY || kind == Field_kind::RAILWAY){
       std::cout << "USAO U SUPPLY ILI RAILWAY" << std::endl;
       auto owner = curr_field_->get_owner();
       if (owner == Owner::BANK){
                   emit stepped_on_property();
            }
        else if(id_to_owner(curr_player_->get_id()) != curr_field_->get_owner())
            {
                emit stepped_on_another_players_property();
            }
        else
            {
               if(curr_field_->is_mortgaged())
                {
                    emit do_you_want_to_lift_the_mortgage();
                }
                else
                {
                    emit stepped_on_previously_bought_field();
                }
            }
        }
  else{
            std::cout<<"USAO U ALL OTHERS"<<std::endl;
       }
 }
void Game::change_current_player()
{
  int preth;
  if(!pocetak_igre)
  {
    int id_of_current_player=curr_player_->get_id();
   if (id_of_current_player==0){
       if(bot1_about->scene()!=nullptr){
       board_->scene_->removeItem(bot1_about);
       }
       if(bot2_about->scene()!=nullptr){
       board_->scene_->removeItem(bot2_about);
       }
       if(bot3_about->scene()!=nullptr){
       board_->scene_->removeItem(bot3_about);
       }
   }

   if (id_of_current_player==1){
       bot1_about->setMsg(notification_string_);
       board_->scene_->addItem(bot1_about);
   }
   if (id_of_current_player==2){
       bot2_about->setMsg(notification_string_);
       board_->scene_->addItem(bot2_about);
   }
   if (id_of_current_player==3){
       bot3_about->setMsg(notification_string_);
       board_->scene_->addItem(bot3_about);
   }
   if(!curr_player_->is_bankrupted()){
       order.push(curr_player_);
   }
   else {
       board_->getCurrentPlayer(curr_player_->get_id())->hide_token();
   }
  }
  notification_string_.clear();
  if(!pocetak_igre){
    preth = curr_player_->get_id();
  }
  if(order.size()==1){  // only human is left and he won
      emit player_bankrupted();
  }
  curr_player_= order.front();
  int i = curr_player_->get_id();
  if(!pocetak_igre){
        if(preth==0){
          QObject::disconnect(getBoard()->getCurrentPlayer(preth),SIGNAL(finished_animation_for_player()), this, SLOT(play_game()));
          QObject::disconnect(getBoard()->getCurrentPlayer(preth),SIGNAL(finished_back_animation()), this, SLOT(play_game()));
        }
        else{
        QObject::disconnect(getBoard()->getCurrentPlayer(preth),SIGNAL(finished_jail_animation()), this, SIGNAL(next_player()));
        QObject::disconnect(getBoard()->getCurrentPlayer(preth),SIGNAL(finished_animation_for_player()), this, SLOT(play_game_for_bots()));
        QObject::disconnect(getBoard()->getCurrentPlayer(preth),SIGNAL(finished_back_animation()), this, SLOT(play_game_for_bots()));
        }
        QObject::disconnect(getBoard(), SIGNAL(dice_animation_finished()), getBoard()->getCurrentPlayer(preth), SLOT(animate_player()));
        QObject::disconnect(this, SIGNAL(stepped_on_go_to_jail()),getBoard()->getCurrentPlayer(preth), SLOT(go_to_jail()));
        QObject::disconnect(this, SIGNAL(go_back_three_spaces()),getBoard()->getCurrentPlayer(preth),SLOT(go_back_three_spaces()));
        QObject::disconnect(this, SIGNAL(move_by_card()), getBoard()->getCurrentPlayer(preth),SLOT(animate_player()));
  }
  else
    pocetak_igre=false;

  order.pop();

  if (i!=0)
      emit hocu_move_player_on_the_board();
  else{
     emit enable_button("Roll the dice!");
   }

}

void Game::move_player_on_the_board()
{
  int i = curr_player_->get_id();
  if(i!=0){
      if (curr_player_->check_jail()){
         in_jail(curr_player_);
      }
      emit bots_is_rolling_the_dice();
  }
  auto dices = board_->get_generated_dice_numbers();
  int steps = dices.first + dices.second;
  board_->getCurrentPlayer(i)->setAdvance(steps);
  std::cout<<"Player advances " <<steps <<std::endl;
  QObject::connect(getBoard(), SIGNAL(dice_animation_finished()), getBoard()->getCurrentPlayer(i), SLOT(animate_player()));
  if(i!=0){
        QObject::connect(getBoard()->getCurrentPlayer(i),SIGNAL(finished_animation_for_player()), this, SLOT(play_game_for_bots()));
  }
  else{
        QObject::connect(getBoard()->getCurrentPlayer(i),SIGNAL(finished_animation_for_player()), this, SLOT(play_game()));
  }
  QObject::connect(this, SIGNAL(move_by_card()), getBoard()->getCurrentPlayer(i),SLOT(animate_player()));
  QObject::connect(this, SIGNAL(stepped_on_go_to_jail()),getBoard()->getCurrentPlayer(i), SLOT(go_to_jail()));
  QObject::connect(this, SIGNAL(go_back_three_spaces()),getBoard()->getCurrentPlayer(i),SLOT(go_back_three_spaces()));

  if(i!=0){
    QObject::connect(getBoard()->getCurrentPlayer(i),SIGNAL(finished_back_animation()), this, SLOT(play_game_for_bots()));
    QObject::connect(getBoard()->getCurrentPlayer(i),SIGNAL(finished_jail_animation()), this, SIGNAL(next_player()));
  }
  else{
      QObject::connect(getBoard()->getCurrentPlayer(i),SIGNAL(finished_back_animation()), this, SLOT(play_game()));
  }
}
void Game::play_game_for_bots(){

   int id = curr_player_->get_id();
    if(throw_dices_again){
       advance_player(*curr_player_, getBoard()->getCurrentPlayer(id)->advance());
        notification_string_.append("Current budget of " + curr_player_->get_name() + " is " + QString::number(curr_player_->get_budget()) + "€.\n");	    
    }
    else{
        throw_dices_again=true;
    }

   unsigned int field_id = getBoard()->getCurrentPlayer(id)->position();
   curr_field_ = board_->get_field_by_id(field_id);
   QString name = curr_player_->get_name();
   Field_kind kind = curr_field_->get_kind();
   //-------------------Checking to see where did the  player land--------------------------
   if(kind == Field_kind::GO_TO_JAIL){
            std::cout << "USAO U GO TO JAIL" << std::endl;
            notification_string_.append( curr_player_->get_name() + " is in jail.");
            emit stepped_on_go_to_jail();
    }
   else if(kind == Field_kind::CHANCE || kind == Field_kind::COMMUNITY){
            std::cout << "USAO U CHANCE ILI COMMUNITY" << std::endl;
            step_on_chance_community_field(curr_player_, *curr_field_);
   }
  else if(kind == Field_kind::PROPERTY){

            std::cout << "PROPERTY" <<std::endl;
            std::cout<< name.toStdString() << " stepped on " << curr_field_->get_name().toStdString() << std::endl;
            Owner owner = curr_field_->get_owner();
            if (owner == Owner::BANK){

                step_on_owner_bank(*curr_field_,curr_player_);

            }
            else if(id_to_owner(curr_player_->get_id()) != curr_field_->get_owner()){
                step_on_owner_other_player(*curr_field_, curr_player_, owner);

            }
           else if(!curr_field_->is_mortgaged() || (curr_field_->is_mortgaged() && lift_mortgage(*curr_field_, curr_player_) )){
                auto bot_player =  dynamic_cast<Bot*>(curr_player_);
                if(bot_player!=nullptr){
                    Decision decision = bot_player->decide_for_property(*curr_field_);
                        if(decision==Decision::sell_property){
                            selling_property(*curr_field_, *curr_player_);
                        }
                        else if (decision==Decision::sell_all_estates) {
                               sell_all_estates();
                        }
                        else if (decision==Decision::sell_estate){
                             sell_one_estate();
                        }
                        else if(decision==Decision::build){
                            build_estate();
                        }
                        else if(decision==Decision::mortgage){
                            mortgage_property(*curr_field_, *curr_player_);
                        }
                        else{
                            notification_string_.append(curr_player_->get_name() + " decided to do nothing.");
                        }
                }

            }
               emit next_player();
        }

        else if(kind == Field_kind::TAX){
          std::cout << "USAO U TAX" << std::endl;
          step_on_tax_field(curr_player_);
          emit next_player();
        }
        else if(kind == Field_kind::SUPPLY || kind == Field_kind::RAILWAY){
            std::cout << "USAO U SUPPLY ILI RAILWAY" << std::endl;
            auto owner = curr_field_->get_owner();
            if (owner == Owner::BANK){
                step_on_owner_bank(*curr_field_,curr_player_);
            }

            else if(id_to_owner(curr_player_->get_id()) != curr_field_->get_owner()){
                step_on_owner_other_player(*curr_field_, curr_player_, owner);
            }

            else if(!curr_field_->is_mortgaged() || (curr_field_->is_mortgaged() && lift_mortgage(*curr_field_, curr_player_) )){
                    auto bot_player =  dynamic_cast<Bot*>(curr_player_);
                    if(bot_player!=nullptr){
                        Decision decision = bot_player->decide_for_property(*curr_field_);
                            if(decision==Decision::sell_property){
                                selling_property(*curr_field_, *curr_player_);
                            }
                            else if(decision==Decision::mortgage){
                                mortgage_property(*curr_field_, *curr_player_);
                            }
                            else
                                notification_string_.append(curr_player_->get_name() + " decided to do nothing.");
                    }

                }
                    emit next_player();
       }
       else{
            std::cout<<"USAO U ALL OTHERS"<<std::endl;
            if (curr_field_->get_id() == 10)
                notification_string_.append(curr_player_->get_name() + " is in visit in jail. ");
            else if (curr_field_->get_id() == 20)
                notification_string_.append(curr_player_->get_name() + " is on parking spot. ");

             emit next_player();
        }
}
