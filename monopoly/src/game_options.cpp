#include "../include/game.hpp"

static qint32 number_of_properties = 0;
static qint32 curr_id_to_draw_on = -1;

void Game::ask_about_previously_bought_field()
{
    QGraphicsTextItem *question = new QGraphicsTextItem("Do you want to: ");
    question->setTextWidth(-1);
    question->setPos(1680, 575);
    board_->scene_->addItem(question);

    Field_kind kind = curr_field_->get_kind();

    Rectangle_with_text *sell_property_option = new Rectangle_with_text("sell this property?",   Qt::darkGreen,1680, 650,100,30);
    board_->scene_->addItem(sell_property_option);

    Rectangle_with_text *mortgage_property_option = new Rectangle_with_text("mortgage this property?",   Qt::darkGreen,1680, 700,100,30);
    board_->scene_->addItem(mortgage_property_option);

    Rectangle_with_text *do_nothing_option = new Rectangle_with_text("do nothing?",   Qt::darkGreen,1680, 750,100,30);
    board_->scene_->addItem(do_nothing_option);

    QObject::connect(sell_property_option, SIGNAL(i_want_to_sell_property()), this, SLOT(sell_property()));

    QObject::connect(mortgage_property_option, SIGNAL(i_want_to_mortgage_this_property()), this, SLOT(do_the_mortgaging_on_current_field()));

    QObject::connect(do_nothing_option, SIGNAL(i_want_to_do_nothing()), this, SLOT(remove_ask_about_previously_bought_field()));

    if(kind == Field_kind::PROPERTY)
    {
        if(curr_field_->get_num_of_houses() < 4)
        {
            Rectangle_with_text *build_house_option = new Rectangle_with_text("build a house here?",  Qt::darkGreen,1680, 800,100,40);
            board_->scene_->addItem(build_house_option);

            QObject::connect(build_house_option, SIGNAL(i_want_to_build_house()), this, SLOT(build_estate()));

        }
        else if(!curr_field_->has_hotel())
        {

            Rectangle_with_text *build_hotel_option = new Rectangle_with_text("build a hotel here?",  Qt::darkGreen,1680, 800,100,40);
            board_->scene_->addItem(build_hotel_option);

            QObject::connect(build_hotel_option, SIGNAL(i_want_to_build_hotel()), this, SLOT(build_estate()));

        }

        if(curr_field_->get_num_of_houses() > 0 || curr_field_->has_hotel())
        {
            Rectangle_with_text *sell_estate_option = new Rectangle_with_text("sell estate?",   Qt::darkGreen,1680, 850,100,30);
            board_->scene_->addItem(sell_estate_option);

            QObject::connect(sell_estate_option, SIGNAL(i_want_to_sell_estate()), this, SLOT(ask_about_selling_estate()));
        }
    }
}
void Game::remove_ask_about_previously_bought_field()
{
    Field_kind kind = curr_field_->get_kind();

    QGraphicsItem *do_you_want_to = board_->view_->itemAt(1680, 575);
    board_->scene_->removeItem(do_you_want_to);
    delete do_you_want_to;

    QGraphicsItem* sell_property_option = board_->view_->itemAt(1680, 650);
    board_->scene_->removeItem(sell_property_option);
    delete sell_property_option;

    QGraphicsItem* mortgage_property_option = board_->view_->itemAt(1680, 700);
    board_->scene_->removeItem(mortgage_property_option);
    delete mortgage_property_option;

    QGraphicsItem* do_nothing_option = board_->view_->itemAt(1680, 750);
    board_->scene_->removeItem(do_nothing_option);
    delete do_nothing_option;

    if(kind == Field_kind::PROPERTY)
    {
        if(curr_field_->get_num_of_houses() < 4 || !curr_field_->has_hotel())
        {
            QGraphicsItem* build_here_option = board_->view_->itemAt(1680, 800);
            board_->scene_->removeItem(build_here_option);
            delete build_here_option;
        }

        if(curr_field_->get_num_of_houses() > 0 || curr_field_->has_hotel())
        {
            QGraphicsItem* sell_estate_option = board_->view_->itemAt(1680, 850);
            board_->scene_->removeItem(sell_estate_option);
            delete sell_estate_option;
        }
    }
}
void Game::do_the_mortgaging_on_current_field()
{
    remove_ask_about_previously_bought_field();

    mortgage_property(*curr_field_, *curr_player_);
    change_color_to_black_when_mortgaged(curr_field_->get_name());

    change_budget_rect();
}
void Game::sell_property()
{
    remove_ask_about_previously_bought_field();

    bool check = curr_player_->has_estates(curr_field_->get_color());
    if(!check){
        curr_player_->sell_property(*curr_field_);
        bank_.buy_property(*curr_field_);

        change_budget_rect();

        QString name = curr_field_->get_name();
        emit sell_property_and_remove_from_owned_properties(name);
    }
    else
    {
        set_top_notif_and_okay("You can't sell this property because you either have estates on it "
                               "or other properties from the same color group have estates or mortgage on them");
    }
}
void Game::remove_property_from_owned_properties(QString name)
{
    auto property= curr_player_->find_rectangle_with_text(name);
    if(property!=nullptr){
            number_of_properties--;
            curr_id_to_draw_on=property->get_id_to_draw();
            board_->scene_->removeItem(property);
            curr_player_->remove_rectangle_with_text(name);
     }
    else{
        std::cout<<"There is no rectangle with text with the name: "<<name.toStdString()<<std::endl;
    }
}
void Game::build_estate()
{
    int id = curr_player_->get_id();

    if(id==0){
        remove_ask_about_previously_bought_field();
    }
    if(curr_player_->can_build_sell(*curr_field_, true))
    {
        if(curr_field_->get_num_of_houses() < 4)
        {
            if (!bank_.sell_house(curr_field_->get_house_price()))
            {
                if(id==0)
                     set_top_notif_and_okay("There are no houses available for sale, please wait another turn");
                else
                     notification_string_.append(curr_player_->get_name() + " can't build house because there are no houses available for sale. ");
            }
            else
            {
                notification_string_.append(curr_player_->get_name() + " built a house. "); // DONE
                curr_field_->build_house();
                curr_player_->update_num_estates(true);
            }

        }
        else if(!curr_field_->has_hotel())
        {
            if (!bank_.sell_hotel(curr_field_->get_hotel_price()))
            {
                if(id==0)
                        set_top_notif_and_okay("There are no hotels available for sale, please wait another turn");
                else
                        notification_string_.append(curr_player_->get_name() + " can't build hotel because there are no hotels available for sale. ");
            }
            else
            {
                notification_string_.append(curr_player_->get_name() + " built a hotel. "); // DONE
                curr_field_->build_hotel();
                curr_player_->update_num_estates(false);
            }
        }
    }
    else
    {
        if(id!=0){
          notification_string_.append(curr_player_->get_name() + " tried to build but not all requirements were fulfilled. "); // DONE
        }
         else{
            QString str = "You can't build here because you either:\n"
                          "a) have mortgaged property in color group of this field\n"
                          "b) aren't building evenly\nor\n"
                          "c) don't have all properties of same color group in possession";
            set_top_notif_and_okay(str);
        }
    }
}
void Game::ask_about_selling_estate()
{
    remove_ask_about_previously_bought_field();

    QGraphicsTextItem *question = new QGraphicsTextItem("Do you want to sell all estates or one estate?");
    question->setTextWidth(210);
    question->setPos(1640, 575);
    board_->scene_->addItem(question);

    Rectangle_with_text *sell_all_estates = new Rectangle_with_text("all estates",   Qt::darkGreen,1680, 650,100,30);
    board_->scene_->addItem(sell_all_estates);

    Rectangle_with_text *sell_one_estate= new Rectangle_with_text("one estate",   Qt::darkGreen,1680, 700,100,30);
    board_->scene_->addItem(sell_one_estate);

    QObject::connect(sell_all_estates, SIGNAL(i_want_to_sell_all_estates()), this, SLOT(sell_all_estates()));
    QObject::connect(sell_one_estate, SIGNAL(i_want_to_sell_one_estate()), this, SLOT(sell_one_estate()));
}
void Game::sell_all_estates(){

    int id = curr_player_->get_id();
    if(id==0)
        remove_ask_about_selling_estates();

    auto clr_group = curr_player_->return_color_group(curr_field_->get_color());
    for (auto f :clr_group){
        bool house = curr_player_->sell_estate(*curr_field_);
        if(house){
            bank_.buy_estate(f->get_house_price(), house);
        }
        else{
            bank_.buy_estate(f->get_hotel_price(), house);
        }

    }
    if(id==0){
        QString str = "If the number of your estates hasn't decreased then:\n"
                      "a) you have mortgaged property in color group of this field\n"
                      "b) you aren't building evenly\nor\n"
                      "c) you don't have all properties of same color group in possession";
        set_top_notif_and_okay(str);
    }
    else{
         notification_string_.append(curr_player_->get_name() + " is selling all of his estates. ");
    }

}
void Game::sell_one_estate(){

    int id = curr_player_->get_id();
    if(id==0)
         remove_ask_about_selling_estates();

    auto clr_group = curr_player_->return_color_group(curr_field_->get_color());
    Field* chosen_field = curr_field_;
    auto number_of_houses = curr_field_->get_num_of_houses();
    if(curr_field_->has_hotel())
    {
        number_of_houses = 5;
    }
    for(auto f : clr_group)
    {
        auto f_number_of_houses = f->get_num_of_houses();
        if(f->has_hotel())
        {
            f_number_of_houses = 5;
        }

        if(f_number_of_houses > number_of_houses)
        {
            chosen_field = f;
            number_of_houses = f_number_of_houses;
        }
    }

    bool house = curr_player_->sell_estate(*chosen_field);
    if(house){
        bank_.buy_estate(chosen_field->get_house_price(), house);
    }
    else{
        bank_.buy_estate(chosen_field->get_hotel_price(), house);
        notification_string_.append(curr_player_->get_name() + " tried to sell one estate but not all requirements are fulfilled. ");
    }
    if(id==0){
    QString str = "If the number of your estates hasn't decreased then:\n"
                  "a) you have mortgaged property in color group of this field\n"
                  "b) you aren't building evenly\nor\n"
                  "c) you don't have all properties of same color group in possession";
    set_top_notif_and_okay(str);
    }
    else{
         notification_string_.append(curr_player_->get_name() + " is selling an estate. Current number of estates is "+chosen_field->get_num_of_houses()+".");
    }

}
void Game::remove_ask_about_selling_estates()
{
    QGraphicsItem *question = board_->view_->itemAt(1680, 575);
    board_->scene_->removeItem(question);
    delete question;

    QGraphicsItem* sell_all_estates = board_->view_->itemAt(1680, 650);
    board_->scene_->removeItem(sell_all_estates);
    delete sell_all_estates;

    QGraphicsItem* sell_one_estate = board_->view_->itemAt(1680, 700);
    board_->scene_->removeItem(sell_one_estate);
    delete sell_one_estate;
}
void Game::pay_to_other_player()
{
    unsigned sum = curr_field_->get_rent();
    Owner owner = curr_field_->get_owner();
    Player *p = get_player_by_id(owner);

    if (!curr_field_->is_mortgaged())
    {
       if(curr_player_->pay_to_player(p, sum))
       {
            std::cout<<"You paid " << sum << " to player " << p->get_name().toStdString()<<std::endl;
            notification_string_.append(curr_player_->get_name() + " paid to " + p->get_name() + sum + "€. ");

            change_budget_rect();

            QString str = "You paid " + QString::number(sum) + "€ to another player" ;
            set_top_notif_and_okay(str);
       }
       else
       {
           QGraphicsTextItem *mortgage_text = new QGraphicsTextItem("You have to mortgage some property because you don't have enough money "
                                                                    "to pay debt to another player. Please choose a property to mortgage on the "
                                                                    "list of properties you own by doing a double mouse click on it! "
                                                                    "If you don't mortgage any property you'll go bankrupt.");
           mortgage_text->setTextWidth(220);
           mortgage_text->setPos(1640, 575);
           board_->scene_->addItem(mortgage_text);

           Rectangle_with_text *not_mortgaging = new Rectangle_with_text("I don't want to mortgage property",Qt::darkRed,1680, 750,100,30);
           board_->scene_->addItem(not_mortgaging);

           QObject::connect(not_mortgaging, SIGNAL(im_not_mortgaging()), this, SLOT(remove_not_mortgaging()));

           curr_player_->disable_all_rectangles_with_text(false);

           QObject::connect(this, SIGNAL(send_field_to_mortgage(Field&,Player&)), this, SLOT(mortgage_property(Field&,Player&)));
           QObject::connect(this, SIGNAL(finished_with_mortgaging_property(int)), this, SLOT(pay_after_mortgaging(int)));
           QObject::connect(this, SIGNAL(finished_with_paying_after_mortgaging()), this, SLOT(remove_mortgage_question()));
       }
    }
    else
    {
        QString str = "This property is mortgaged, so you\n"
                      "don't have to pay to other player\n"
                      "for stepping on it";
        set_top_notif_and_okay(str);
    }
}


void Game::ask_about_buying()
{
    if(curr_player_->get_budget() >= curr_field_->get_price())
    {
        QGraphicsTextItem *question = new QGraphicsTextItem("Do you want to buy this property?");
        question->setTextWidth(-1);
        question->setPos(1640, 575);

        Rectangle_with_text *yes_option = new Rectangle_with_text("I want to buy it",   Qt::darkGreen,1680, 650,100,30);
        Rectangle_with_text *no_option = new Rectangle_with_text("I don't want to buy it",Qt::darkRed,1680, 750,100,30);

        board_->scene_->addItem(question);
        board_->scene_->addItem(yes_option);
        board_->scene_->addItem(no_option);

        QObject::connect(yes_option, SIGNAL(i_want_to_buy_it()), this, SLOT(buy_property()));
        QObject::connect(no_option, SIGNAL(i_dont_want_to_buy_it()), this, SLOT(dont_buy_property()));
    }
    else
    {
        QGraphicsTextItem *not_enough_money = new QGraphicsTextItem("You don't have enough money to buy this property");
        not_enough_money->setTextWidth(210);
        not_enough_money->setPos(1640, 575);

        Rectangle_with_text *okay = new Rectangle_with_text("Okay",   Qt::darkGreen,1680, 650,100,30);

        board_->scene_->addItem(not_enough_money);
        board_->scene_->addItem(okay);

        QObject::connect(okay, SIGNAL(okay_i_dont_have_money_to_buy()), this, SIGNAL(finished_tell_about_not_having_money()));
    }
}
void Game::buy_property()
{
    step_on_owner_bank(*curr_field_, curr_player_);

    number_of_properties += 1;
    if(curr_id_to_draw_on==-1){
        curr_id_to_draw_on=number_of_properties;
    }

    Rectangle_with_text *property = new Rectangle_with_text(curr_field_->get_name(),curr_field_->get_color(),1055, 650 + curr_id_to_draw_on*24,245,20,Button_kind::BOUGHT_PROPERTY,curr_id_to_draw_on);
    if(curr_id_to_draw_on>13){
        property->set_x_pos(1300);
        property->set_y_pos(650+(curr_id_to_draw_on-13)*24);
    }
    curr_player_->add_new_rectnagle_with_text(property);

    emit add_to_owned_properties(property->get_text());

    QObject::connect(property, SIGNAL(put_property_under_mortgage(QString)), this, SLOT(change_color_to_black_when_mortgaged(QString)));
    QObject::connect(property, SIGNAL(put_property_under_mortgage(QString)), this, SLOT(got_mortgage_to_get_out_of_jail(QString)));

    QGraphicsItem *question = board_->view_->itemAt(1640, 575);
    board_->scene_->removeItem(question);
    delete question;

    QGraphicsItem* yes_option = board_->view_->itemAt(1680, 650);
    board_->scene_->removeItem(yes_option);
    delete yes_option;

    QGraphicsItem* no_option = board_->view_->itemAt(1680, 750);
    board_->scene_->removeItem(no_option);
    delete no_option;

    change_budget_rect();
}
void Game::dont_buy_property()
{
    QGraphicsItem *question = board_->view_->itemAt(1640, 575);
    board_->scene_->removeItem(question);
    delete question;

    QGraphicsItem* yes_option = board_->view_->itemAt(1680, 650);
    board_->scene_->removeItem(yes_option);
    delete yes_option;

    QGraphicsItem* no_option = board_->view_->itemAt(1680, 750);
    board_->scene_->removeItem(no_option);
    delete no_option;
}
void Game::remove_tell_about_not_having_money()
{
    QGraphicsItem *not_enough_money = board_->view_->itemAt(1640, 575);
    board_->scene_->removeItem(not_enough_money);
    delete not_enough_money;

    QGraphicsItem* okay = board_->view_->itemAt(1680, 650);
    board_->scene_->removeItem(okay);
    delete okay;
}



void Game::remove_chance_community_card_from_scene()
{
    if (chance_community_card_shown_){
    QGraphicsItem *question = board_->view_->itemAt(350, 450);
    board_->scene_->removeItem(question);
    chance_community_card_shown_=false;
    }
}
void Game::activate_jail_free_card_posetion()
{
   QGraphicsRectItem* jail_card=qgraphicsitem_cast<QGraphicsRectItem*>(board_->view_->itemAt(1180,600));

   board_->scene_->removeItem(jail_card);
   jail_card->setBrush(Qt::darkGreen);
   board_->scene_->addItem(jail_card);
}
void Game::display_in_owned_properties(QString name)
{
   auto property= curr_player_->find_rectangle_with_text(name);
   if(property!=nullptr){

        board_->scene_->addItem(property);
        if(curr_id_to_draw_on!=-1)
            curr_id_to_draw_on=-1;

       QObject::connect(property, SIGNAL(show_card_for_the_owned_property(QString)), this->board_, SLOT(owned_property_card(QString)));
       QObject::connect(property, SIGNAL(hide_card_for_the_owned_property(QString)), this->board_, SLOT(owned_property_card(QString)));
   }
   else{
       std::cout<<"There is no rectangle with text with the name: "<<name.toStdString()<<std::endl;
   }

}
