#include "../include/game.hpp"

void Game::ask_about_tax()
{
    QGraphicsTextItem *tax_question = new QGraphicsTextItem("You've stepped on tax field. Do you want to pay 200€ or 10% of your total worth?");
    tax_question->setTextWidth(190);
    tax_question->setPos(1640, 575);

    Rectangle_with_text *option_one = new Rectangle_with_text("200€",Qt::darkGreen,1680, 650,100,30);
    Rectangle_with_text *option_two = new Rectangle_with_text("10% of your total worth",Qt::darkGreen,1680, 750,100,30);

    board_->scene_->addItem(tax_question);
    board_->scene_->addItem(option_one);
    board_->scene_->addItem(option_two);

    QObject::connect(option_one, SIGNAL(tax_option_one()), this, SLOT(take_the_tax_option_one()));
    QObject::connect(option_two, SIGNAL(tax_option_two()), this, SLOT(take_the_tax_option_two()));
}
void Game::take_the_tax_option_one()
{
    curr_player_->set_needed_money(200);

    QGraphicsItem *tax_question = board_->view_->itemAt(1640, 575);
    board_->scene_->removeItem(tax_question);
    delete tax_question;

    QGraphicsItem* option_one = board_->view_->itemAt(1680, 650);
    board_->scene_->removeItem(option_one);
    delete option_one;

    QGraphicsItem* option_two = board_->view_->itemAt(1680, 750);
    board_->scene_->removeItem(option_two);
    delete option_two;

    if(curr_player_->pay_to_bank(200))
    {
        bank_.collect(200);
        change_budget_rect();
    }
    else
    {
        QGraphicsTextItem *tax_mortgage_text = new QGraphicsTextItem("You have to mortgage some property because you don't have enough "
                                                                     "money to pay the amount of 200€. Please choose a property to mortgage on the "
                                                                     "list of properties you own by doing a double mouse click on it! "
                                                                     "If you don't mortgage any property you'll go bankrupt.");
        tax_mortgage_text->setTextWidth(210);
        tax_mortgage_text->setPos(1640, 575);
        board_->scene_->addItem(tax_mortgage_text);

        Rectangle_with_text *not_mortgaging = new Rectangle_with_text("I don't want to mortgage property",Qt::darkRed,1680, 750,100,30);
        board_->scene_->addItem(not_mortgaging);

        QObject::connect(not_mortgaging, SIGNAL(im_not_mortgaging()), this, SLOT(remove_not_mortgaging()));

        curr_player_->disable_all_rectangles_with_text(false);

        QObject::connect(this, SIGNAL(send_field_to_mortgage(Field&,Player&)), this, SLOT(mortgage_property(Field&,Player&)));
        QObject::connect(this, SIGNAL(finished_with_mortgaging_property(int)), this, SLOT(pay_after_mortgaging(int)));
        QObject::connect(this, SIGNAL(finished_with_paying_after_mortgaging()), this, SLOT(remove_mortgage_question()));
    }
}
void Game::take_the_tax_option_two()
{
    QGraphicsItem *tax_question = board_->view_->itemAt(1640, 575);
    board_->scene_->removeItem(tax_question);
    delete tax_question;

    QGraphicsItem* option_one = board_->view_->itemAt(1680, 650);
    board_->scene_->removeItem(option_one);
    delete option_one;

    QGraphicsItem* option_two = board_->view_->itemAt(1680, 750);
    board_->scene_->removeItem(option_two);
    delete option_two;

    int total_worth_before = curr_player_->calculate_total_worth();
    if(total_worth_before == 0)
    {
        total_worth_before = 2000;
    }
    int total_worth = 0;
    if(total_worth_before < 10)
    {
        total_worth = 1;
    }
    else
    {
        total_worth = 0.1*total_worth_before;
    }
    curr_player_->set_needed_money(total_worth);
    if(curr_player_->pay_to_bank(total_worth)){

        bank_.collect(total_worth);
        change_budget_rect();
    }
    else
    {
        QString str = "You have to mortgage some property because you don't have enough money to pay the amount of ";
        QString money = QString::number(0.1 * total_worth_before);
        str.append(money);
        str.append("€. ");
        if(total_worth_before == 2000)
        {
            QString t = "Because you tried to cheat your tax is now 200€!";
            str.append(t);
        }
        str.append("Please choose a property to mortgage on the "
                   "list of properties you own by doing a double mouse click on it! "
                   "If you don't mortgage any property you'll go bankrupt.");
        QGraphicsTextItem *tax_mortgage_text = new QGraphicsTextItem(str);
        tax_mortgage_text->setTextWidth(210);
        tax_mortgage_text->setPos(1640, 575);
        board_->scene_->addItem(tax_mortgage_text);

        Rectangle_with_text *not_mortgaging = new Rectangle_with_text("I don't want to mortgage property",Qt::darkRed,1680, 750,100,30);
        board_->scene_->addItem(not_mortgaging);

        QObject::connect(not_mortgaging, SIGNAL(im_not_mortgaging()), this, SLOT(remove_not_mortgaging()));

        curr_player_->disable_all_rectangles_with_text(false);

        QObject::connect(this, SIGNAL(send_field_to_mortgage(Field&,Player&)), this, SLOT(mortgage_property(Field&,Player&)));
        QObject::connect(this, SIGNAL(finished_with_mortgaging_property(int)), this, SLOT(pay_after_mortgaging(int)));
        QObject::connect(this, SIGNAL(finished_with_paying_after_mortgaging()), this, SLOT(remove_mortgage_question()));
    }
}
