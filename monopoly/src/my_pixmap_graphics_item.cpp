#include "../include/my_pixmap_graphics_item.hpp"
My_pixmap_graphics_item::My_pixmap_graphics_item(const QPixmap & pixmap, QGraphicsItem * parent): QObject(),
                                                 QGraphicsPixmapItem(pixmap, parent){}

void My_pixmap_graphics_item::hide()
{
    this->setVisible(false);
    this->deleteLater();

}
