#include "../include/player.hpp"

Player::Player( unsigned id,QString name, unsigned position,
        int budget)
    : position_(position), budget_(budget), name(name), id(id),
      color_(qrand() % 256, qrand() % 256, qrand() % 256)
    {}

Player::~Player(){}


std::vector<Field*> Player::return_color_group(const QString color){
    std::vector<Field*> color_group;
    std::copy_if(fields_.begin(), fields_.end(), std::back_insert_iterator(color_group),[color](Field* f){return !color.compare(f->get_color());});
    return color_group;
}

//// estates can be built on Field f only if all fields of same color group
//// have same number of houses or if Field f is the only one with one less house than others
//// similarly for selling, Field f is the only one with one more house than others

bool Player::can_build_sell( Field f, const bool build) {


    std::cout<<"USLO U CAN BUILD/SELL"<<std::endl;
    auto clr_group = this->return_color_group(f.get_color());

    if(f.get_kind() == Field_kind::PROPERTY){

        if ((clr_group.size() == 3 && f.get_color() != "mediumpurple" && f.get_color() != "darkblue")
            || (clr_group.size() == 2 && (f.get_color() == "mediumpurple" || f.get_color() == "darkblue"))){

            int num = f.get_num_of_houses();
            //checks if all have the same number of houses as Field f
            bool can = std::all_of(clr_group.cbegin(), clr_group.cend(), [num](Field *f){return f->get_num_of_houses() == num;});

            if (can && num <= 4){
                std::cout<<"all even"<<std::endl;
                return true;
            }
            else{

                for(auto field:clr_group){

                    if(field->get_id() == f.get_id()){
                        continue;
                    }

                    if(field->is_mortgaged()){
                        std::cout<<"You can't build/sell here, some property of the same color group is mortgaged!"<<std::endl;
                        return false;
                    }
                    int n_estates = field->get_num_of_houses();

                    if (build){
                        std::cout<<"BUILD if"<<std::endl;
                        if(num + 1 - n_estates > 1 ){
                            std::cout<<"You can't build here, you must build evenly."<<std::endl;
                            return false;
                        }
                    }
                    else{
                        if( n_estates - (num - 1) > 1 ){
                            std::cout<<"You must sell evenly."<<std::endl;
                            return false;
                        }
                    }

                }
                std::cout<<"ovde"<<std::endl;
                return true;
            }
        }
        std::cout<<"You don't have all field of the color group"<<std::endl;
        return false;

    }
    else{
        std::cout<<"You can't build/sell here! It's not a property."<<std::endl;
        return false;
    }

}

bool Player::sell_estate( Field& f, bool all){

    if (f.has_hotel()){
        f.destroy_hotel();
        --number_of_hotels_;
        budget_ += f.get_hotel_price() / 2;
        return false;
    }

    if(!all){
        bool can = this->can_build_sell(*(&f), false);

        if (can){
            --number_of_houses_;
            budget_ += f.get_house_price() / 2;
            f.destroy_house();
        }
    }
    else{
        std::cout<<"all estates"<<std::endl;
        --number_of_houses_;
        budget_ += f.get_house_price() / 2;
        f.destroy_house();

    }
    return true;
}



void Player::sell_property(Field& f){

    int position = 0;
    int n = fields_.size();
    for (int i =0; i < n; i++){
        if(f.get_id() == fields_[i]->get_id()){
            position = i;
            break;
        }
    }
    budget_ += fields_[position]->get_price();
    fields_.erase(fields_.begin() + position);
    f.change_owner(Owner::BANK);

}

bool Player::has_estates(const QString color) const{

    std::vector<Field*> color_group;
    std::copy_if(fields_.begin(), fields_.end(), std::back_insert_iterator(color_group),[color](Field *f){return !color.compare(f->get_color());});
    bool has = false;
    if (color_group.size() > 1){
        for (auto field:color_group){
            if(field->get_num_of_houses()!=0){
                has = true;
                std::cout<<"Other properties from the same color group have estates! You can't sell this property."<<std::endl;
                return has;
            }
        }
    }

    return  color_group[0]->get_num_of_houses() != 0;

}

void Player::set_number_of_houses(const bool build)
{
    if (build)
        number_of_houses_++;
    else if (get_number_of_houses() > 0)
        number_of_houses_--;
}

void Player::set_number_of_hotels(const bool build)
{
    if (build)
        number_of_hotels_++;
    else if (get_number_of_hotels() > 0 )
            number_of_hotels_--;
}


bool Player::pay_to_bank(int sum)
{
    if(budget_ < sum)
    {
        std::cout << name.toStdString() << " can't pay " << sum <<"€ to Bank" << std::endl;
        return false;
    }

    budget_ -= sum;
    return true;
}

bool Player::pay_mortgage(int mortgage)
{
    if(budget_<=mortgage){
        std::cout<<"You don't have enough money to lift the mortgage!" << std::endl;
        return false;
    }
    budget_-= mortgage;
    return true;
}

void Player::update_num_estates(bool house)
{
    if (house){
        number_of_houses_++;
        return;
    }
    number_of_hotels_++;
}


bool Player::check_jail_card() const {return jail_card;}

void Player::set_jail_card(Field_kind kind){
    jail_card = true;
    jail_card_from = kind;
}

Field_kind Player::unset_jail_card(){
    jail_card = false;
    return jail_card_from;
}



bool Player::pay_to_player(Player *p, const int sum)
{
    if(budget_ < sum){
        std::cout << name.toStdString() << " can't pay " << sum <<"e to " << p->get_name().toStdString()<<std::endl;
        return false;
    }
    budget_ -= sum;
    p->receive_money(sum);
    return true;
}

bool Player::add_property(Field &p)
{
    if(budget_ < p.get_price()){
        return false;
    }
    std::cout<<budget_<<std::endl;
    budget_ -= p.get_price();
    std::cout<<budget_<<std::endl;
    fields_.push_back(&p);
    return true;
}

void Player::sell_all_estates()
{
    for (auto field: fields_){
        if(field->has_hotel()){
            field->destroy_hotel();
        }
        else if(field->get_num_of_houses() !=0){
            for (int i =0; i<field->get_num_of_houses();i++){
                field->destroy_house();
            }
        }
        field->change_owner(Owner::BANK);
    }
}

int Player::calculate_total_worth() const
{
    int total_worth = budget_;
    int sum = 0;
    for(auto field: fields_){
        sum+= field->get_price();
        if(field->has_hotel()){
            sum += field->get_hotel_price();
        }
        else if(field->get_num_of_houses() > 0){
            sum += field->get_num_of_houses() * field->get_house_price();
        }
        sum = 0;
    }

    return total_worth + sum;
}

std::pair<unsigned, bool> Player::move_forward(const int steps){
    // there are 40 fields on the table, indexing starts from 0
    position_ += steps;

    bool pass = false;
    if (position_ == 0 || steps == -1)
    {
        pass = true;
    }

    if (position_ > 39){
        position_ -= 40;
        pass = true;
    }
//    else if (position_ < 0)
//    {
//        position_ += 40;
//        pass = false;
//    }

    return std::pair(position_, pass);

}


void Player::receive_money(const int sum){
    budget_ += sum;
    std::cout<< "Player " << name.toStdString() << " received " << sum << std::endl;
}


void Player::add_new_rectnagle_with_text(Rectangle_with_text *r)
{
  auto name=r->get_text();
  r->blockSignals(true);
  owned_rectangles_with_text_.insert(name,r);
}

void Player::remove_rectangle_with_text(QString rect_text)
{
    owned_rectangles_with_text_.remove(rect_text);
    auto rec= find_rectangle_with_text(rect_text);
    delete rec;

}

Rectangle_with_text* Player::find_rectangle_with_text(QString name){


  QMap<QString,Rectangle_with_text*>::const_iterator iterator=owned_rectangles_with_text_.find(name);
  if (iterator != owned_rectangles_with_text_.end()){
    return iterator.value();
  }
   return nullptr;
}


void Player::disable_all_rectangles_with_text(bool val)
{
    for(auto x: owned_rectangles_with_text_)
    {
        x->blockSignals(val);
    }
}
