#include "../include/welcome.hpp"
#include "../build/ui_welcome.h"

Welcome::Welcome(QString *name, QString *token, QDialog *parent) :
    QDialog(parent),
    ui(new Ui::Welcome)
{
    setWindowState(Qt::WindowFullScreen);

    name_ = name;
    token_ = token;

    tokens.push_back(":/images/duck.png");
    tokens.push_back(":/images/hat.png");
    tokens.push_back(":/images/ship.png");

    ui->setupUi(this);

    connect(ui->pb_back, &QPushButton::clicked, this, &Welcome::back_clicked);
    connect(ui->pb_quit, &QPushButton::clicked, this, &Welcome::quit_clicked);
    connect(ui->pb_next, &QPushButton::clicked, this, &Welcome::next_clicked);
    connect(ui->pb_start, &QPushButton::clicked, this, &Welcome::start_clicked);
    connect(ui->pb_rules, &QPushButton::clicked, this, &Welcome::rules_clicked);

//    button_click = new QMediaPlayer();
//    button_click->setMedia(QUrl("qrc:/sounds/button_click.wav"));
}

void Welcome::paintEvent(QPaintEvent *e)
{
    QPixmap welcome_bckg = QPixmap(":/images/welcome.jpeg");
    QPalette palette;
    welcome_bckg = welcome_bckg.scaled(this->size(), Qt::IgnoreAspectRatio);
    palette.setBrush(QPalette::Window, welcome_bckg);
    this->setPalette(palette);

    QWidget::paintEvent(e);
}

void Welcome::start_clicked(){
//    button_click_sound();
    username = ui->lineEdit->text();

    if (username == "")
    {
        ui->lineEdit->setStyleSheet("background-color: red");
        ui->lineEdit->setPlaceholderText("Blank name");

        return;
    }

    *name_ = username;
    *token_ = human_token;

    this->accept();
}

void Welcome::quit_clicked()
{
//    button_click_sound();
    this->reject();
}

void Welcome::next_clicked(){
//    button_click_sound();

    tokens.push_back(human_token);
    human_token = tokens.front();
    tokens.pop_front();
    ui->token_frame->setStyleSheet("border-image: url(" + human_token + ");");
}
void Welcome::back_clicked(){
//    button_click_sound();

    tokens.push_front(human_token);
    human_token = tokens.back();
    tokens.pop_back();
    ui->token_frame->setStyleSheet("border-image: url(" + human_token + ");");
}

void Welcome::rules_clicked()
{
//    button_click_sound();

    Rules *rules = new Rules();
    rules->exec();
}

void Welcome::button_click_sound() const
{
    if (button_click->state() == QMediaPlayer::PlayingState)
        button_click->setPosition(0);
    else
        button_click->play();
}

void Welcome::keyPressEvent(QKeyEvent *event)
{
   if (event->key() == Qt::Key_Escape)
      return;

   QDialog::keyPressEvent(event);
}

Welcome::~Welcome()
{
    tokens.clear();
    delete ui;
}
